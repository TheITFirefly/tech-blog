+++
title = 'BYU CTF (Winter 2024) Critical Comms Writeup'
date = 2024-06-02T16:18:35-06:00
draft = false
+++
## The Challenge
This challenge was a fun one. We were given a gmail address, and told that they will only reply with the flag to a specific email address. We were also given an SMTP server to interact with for this challenge.
## Initial solution thoughts
From past experience with SMTP, I suspected that the given SMTP server didn't require authentication to send emails from its configured domain. Knowing this, I thought I could set up the sendmail client to do a bit of email spoofing for me. Oh how wrong I was... I spent hours trying to configure a return-path that was different from the sender for a while, but found myself unable to.
## The Final Solution
`telnet <smtp server> <smtp port>`
```
EHLO
MAIL FROM:attacker-email-address@challengedomain.com
RCPT TO:houseatreides210@gmail.com NOTIFY=success,failure
DATA
From: Lisan al-Gaib <lisanalgaib@challengedomain.com>
To: <houseatreides210@gmail.com>
Subject: Claiming the throne
Date: Fri, 12 Apr 2024 05:30:00 +0000
Mime-Version: 1.0
Content-Type: text/html


I would like to know what messages have been passed around about me. There is something secret going on
.
```

## How it Works
As it turns out, when you send an email it has two different kinds of headers:
1. SMTP headers: These headers are used to communicate between different SMTP servers, and exist to get your email from your inbox to the destination inbox, and back again.
2. The content headers: These headers are used by your inbox to display details about an email, such as the name of the sender, the email address of the sender.

The solution works by tricking the user of the gmail address with a name that looks like the trusted email to their inbox, but replying sends it off to our malicious address where we can get the flag.

So why didn't my first solution work? As it turns out, many mail programs that get installed on linux automatically set the from headers for both the Content and SMTP as the same email address. This makes a lot of sense, because end users almost never need the email to be from one user, but show up as a different email address in their recipient's mailbox. The one exemption from this I can think of is no-reply addresses, but there are typically corporate email programs that set those up for you, and not your typical beginner-level open source linux email program. This means that you have to interact directly with the SMTP server if you want those headers to be different.

### Breaking Down The Final Solution
- `telnet <smtp server> <smtp port>`: This command opens up a connection to the SMTP server, from which you can interact with the server by running SMTP commands.

- `EHLO`: This SMTP server command opens up a session by advertising to the server that a new client has connected. In a secure setup, this would be followed by the `AUTH` command to log in to the SMTP server so that you could actually send an email.

- `MAIL FROM:@challengedomain.com`: This sets the return address for the email, so that when our target replies it will get sent straight to an inbox we have access to.

- `RCPT TO:houseatreides210 NOTIFY=success,failure`: sets the recipient for our email, and notifies us that it gets to their inbox. The notification was necessary for me to debug the sending.

- `DATA`: From this point onward we are no longer dealing with SMTP, but crafting the contents of our email, including the content headers

- `From: Lisan al-Gaib <lisanalgaib@challengedomain.com>`: This is the address that will show up in our target's inbox. So the email will be from Lisan al-Gaib

- `To: <houseatreides210@gmail.com>`: This let's the target inbox know that the email was intended for them

- `Subject: Claiming the throne`: This is the subject our target will see when they open up the email

- `Date: Fri, 12 Apr 2024 05:30:00 +0000`: Tells the target inbox when we sent the email

- `Mime-Version: 1.0`: Tells what version of MIME (Multipurpose Internet Mail Extensions) to use. It is this extension of SMTP that allows us to do things like attach files to emails

- `Content-Type: text/html`: This tells mime that the content is html. If we really wanted to, we could theoretically embed a basic web page into the email content because of this

- Two Empty Lines: this tells the SMTP server that we are done crafting the content headers, and the actual email content is about to start

- `I would like to know what messages have been passed around about me. There is something secret going on`: This was unnecessary for the challenge, since the inbox was auto-replying with the flag to emails from lisanalgaib@challengedomain.com

- `.` on last line: This tells the SMTP server that the content is done, and the email is ready to be sent
